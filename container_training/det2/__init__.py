"""
Train FasterRCNN and Retinanet models on General COCO Dataset:

* Build deep learning models by using Detectron2 (https://detectron2.readthedocs.io/)
* Pretrained models at https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md
* This module has an entry_point to be used with SageMaker training jobs (see 'training.py')
"""
