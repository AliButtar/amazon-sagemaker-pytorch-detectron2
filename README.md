## Object Detection with Detectron2 on Amazon SageMaker

### Overview

In this repository, we use [Amazon SageMaker](https://aws.amazon.com/sagemaker/) to build, train and deploy [Faster-RCNN](https://arxiv.org/abs/1506.01497) and [RetinaNet](https://arxiv.org/abs/1708.02002) models using [Detectron2](https://github.com/facebookresearch/detectron2).
Detectron2 is an open-source project released by Facebook AI Research and build on top of PyTorch deep learning framework. Detectron2 makes easy to build, train and deploy state of the art object detection algorithms. Moreover, Detecron2’s design makes easy to implement cutting-edge research projects without having to fork the entire codebase.Detectron2 also provides a [Model Zoo](https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md) which is a collection of pre-trained detection models we can use to accelerate our endeavour.

This repository shows how to do the following:

* Build Detectron2 Docker images and push them to [Amazon ECR](https://aws.amazon.com/ecr/) to run training and inference jobs on Amazon SageMaker.
* Before uploading the data, the "path" key inside the COCO Annotations file needs to be changed for `training`, `validation`, and `test` set as following: 
```
Before Example:
{
      "id": 309,
      "path": "new_set_00000803.jpg",
      "width": 800,
      "height": 718,
      "file_name": "new_set_00000803.jpg"
}

After Example:
{
      "id": 309,
      "path": "../validation/new_set_00000803.jpg",
      "width": 800,
      "height": 718,
      "file_name": "new_set_00000803.jpg"
}
```
* Upload data on S3 in a bucket in the following format:
```
<bucket-name>
    detectron2:
        data:
           training-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           validation-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           test-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           annotations:
                training.json
                validation.json
                test.json
           inference-script:
                predict_det2.py
                predict_det2_sahi.py
    

Example:

photogauge-sagemaker-doors-detectron:
    detectron2:
        data:
           training-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           validation-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           test-images:
               img1.jpg
               img2.jpg
               ...
               img999.jpg
           annotations:
                training.json
                validation.json
                test.json
           inference-script:
                predict_det2.py
                predict_det2_sahi.py
```

* Run a SageMaker Training job to finetune pre-trained model weights on a custom dataset.
* Configure SageMaker Hyperparameter Optimization jobs to finetune hyper-parameters.
* Run a SageMaker Batch Transform job to predict bouding boxes in a large chunk of images or deploy a model for real-time inference through lambda and gateway.

### Get Started

* Create a SageMaker notebook instance with an EBS volume equal or bigger than 5 GB ( can be scaled up if needed to push docker containers)

* OPTIONAL STEP only if changes are needed to be made to training and inference code that is pushed to ECR then add the following lines to **create notebook** section of your life cycle configuration:

```
service docker stop
sudo mv /var/lib/docker /home/ec2-user/SageMaker/docker
sudo ln -s /home/ec2-user/SageMaker/docker /var/lib/docker
service docker start
```

This ensures that docker builds images to a folder that is mounted on EBS. Once the instance is running, open Jupyter lab, launch a terminal and clone this repository:

```
cd SageMaker
git clone https://AliButtar@bitbucket.org/AliButtar/amazon-sagemaker-pytorch-detectron2.git
cd amazon-sagemaker-pytorch-detectron2
```
Open the [notebook](d2_general_sagemaker.ipynb). Follow the instruction in the notebook to Train, Optimize and Deploy the model.

URI's for Training and Inference Images on AWS ECR:

Training: `592891983137.dkr.ecr.us-east-1.amazonaws.com/sagemaker-d2-general:latest`

Inference: `592891983137.dkr.ecr.us-east-1.amazonaws.com/sagemaker-d2-serve:sahi`

